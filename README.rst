Sentinel Tool
======================================

Create virtual env:

.. code-block:: bash

   python3 -m venv venv
   . venv/bin/activate

Install requirements:

.. code-block:: bash

   python3 -m pip install -r requirements/dev.txt

Run:

.. code-block:: bash

   python3 -m app --config=tests/config.yaml