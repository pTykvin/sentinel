import logging

import click

from app.core.application_context import load_context


# TODO: extract logging configuration to the file. Setup logging file appender
from app.util.exception_handler import err_exit


def init_logging(verbose, silent):
    if silent:
        level = logging.ERROR
    elif verbose:
        level = logging.DEBUG
    else:
        level = logging.INFO

    logging.basicConfig(
        level=level,
        format="%(asctime)s - [%(levelname)s] - (%(filename)8s).%(funcName)s(%(lineno)d): %(message)s",
    )


@click.command()
@click.option('-f', '--file', 'config_file',
              show_default=True,
              default='../tests/config.yaml',
              type=click.Path(),
              help='Relative path to the configuration file')
@click.option('-m', '--mode',
              show_default=True,
              default='history',
              type=click.Choice(['inspect', 'history', 'recovery'], case_sensitive=True),
              help='Execution mode. ')
@click.option('-v', '--verbose',
              default=False,
              count=True,
              help='Enable verbose output for the given subsystem')
@click.option('-s', '--silent',
              default=False,
              count=True,
              help='Silent mode')
def main(config_file, mode, verbose, silent):
    init_logging(verbose, silent)
    context = load_context(config_file)

    if mode == 'inspect':
        context.git_wrapper.inspect()
    elif mode == 'history':
        context.git_wrapper.log()
    elif mode == 'recovery':
        # TODO: Need to implement recovery interactive CLI
        err_exit('Not implemented!')


if __name__ == '__main__':
    main()
