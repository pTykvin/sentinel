from pathlib import Path

from app.util.exception_handler import err_exit


def file_safe(path):
    file = Path(path)
    if not file.is_file():
        err_exit(f"{path} is not a file")
    return path
