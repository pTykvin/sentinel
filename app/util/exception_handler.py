import sys

import logging


def err_exit(ex):
    if hasattr(ex, 'message'):
        logging.fatal(ex.message)
    else:
        logging.fatal(ex)
    sys.exit(-1)
