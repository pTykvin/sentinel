from dataclasses import dataclass

from app.service.git_wrapper import GitWrapper
from app.service.properties_parser import YamlPropertiesParser
from app.util.file_loader import file_safe
import logging


# TODO: research dependency injection framework
def bootstrap(config_file):
    logging.info("Bootstrap...")
    parser = YamlPropertiesParser(file_safe(config_file))
    git_wrapper = GitWrapper(parser)
    return BootstrapContainer(git_wrapper, parser)


@dataclass
class BootstrapContainer:
    git_wrapper: GitWrapper
    properties_parser: YamlPropertiesParser
