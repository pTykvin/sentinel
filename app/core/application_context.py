from dataclasses import dataclass

from app.service.git_wrapper import GitWrapper
from app.service.properties_parser import YamlPropertiesParser
from app.util.file_loader import file_safe
import logging


def load_context(config_file):
    logging.debug("Application context is loading...")
    parser = YamlPropertiesParser(file_safe(config_file))
    git_wrapper = GitWrapper(parser)
    result = ApplicationContext(git_wrapper, parser)
    logging.info("Application context is loaded.")
    return result


@dataclass
class ApplicationContext:
    git_wrapper: GitWrapper
    properties_parser: YamlPropertiesParser
