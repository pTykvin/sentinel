from array import array
from dataclasses import dataclass
from pathlib import Path

import yaml
import logging

from app.util.exception_handler import err_exit


@dataclass
class Properties:
    root: Path
    ignores: array


class YamlPropertiesParser:

    def __init__(self, properties_file: Path):
        self.properties_file = properties_file
        self.properties_home = None
        self.properties = None

    def load(self):
        if self.properties:
            logging.debug(f"Properties already parsed. Just return")
            return self.properties

        logging.debug(f"Loading the yaml configuration: file={self.properties_file}...")

        try:
            with open(self.properties_file, "r") as stream:
                try:
                    yaml_properties = yaml.safe_load(stream)
                    self.properties_home = Path(self.properties_file).parent.resolve()
                    self.properties = self.parse(yaml_properties)
                    logging.info(f"Configuration is successfully loaded.")
                    return self.properties
                except yaml.YAMLError as exc:
                    err_exit(exc)
        except Exception as ex:
            err_exit(ex)

    def parse(self, yaml_properties):
        logging.debug(f"Parsing {yaml_properties}...")

        root_dir = Path(self.properties_home, yaml_properties['watch']['root'])
        if not root_dir.exists() or not root_dir.is_dir():
            err_exit(f"Directory {root_dir} doesn't exists")

        ignores = yaml_properties['watch']['ignores']
        result = Properties(root_dir, ignores)
        logging.debug(f"Parsing result is {result}.")
        return result
