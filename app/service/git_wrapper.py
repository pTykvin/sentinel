import fnmatch
import logging
from datetime import datetime
from pathlib import Path

import pygit2
from pygit2 import Signature, GIT_STATUS_WT_NEW, GIT_STATUS_INDEX_NEW, GIT_STATUS_INDEX_MODIFIED, \
    GIT_STATUS_WT_MODIFIED, GIT_STATUS_INDEX_RENAMED, GIT_STATUS_WT_RENAMED, GIT_STATUS_INDEX_DELETED, \
    GIT_STATUS_WT_DELETED, GIT_STATUS_CURRENT


def map_status(status):
    if status in [GIT_STATUS_INDEX_NEW, GIT_STATUS_WT_NEW]:
        return "CREATED"
    if status in [GIT_STATUS_INDEX_MODIFIED, GIT_STATUS_WT_MODIFIED]:
        return "MODIFIED"
    if status in [GIT_STATUS_INDEX_RENAMED, GIT_STATUS_WT_RENAMED]:
        return "RENAMED"
    if status in [GIT_STATUS_INDEX_DELETED, GIT_STATUS_WT_DELETED]:
        return "DELETED"
    return "UNEXPECTED"


def create_commit_message(files_statuses):
    message = ""
    for filepath in files_statuses:
        message += f"[{files_statuses[filepath]}] {filepath}\n"
    return message.strip()


class GitWrapper:
    me = Signature('Sentinel Tool', 'sentinel@tool.tld')

    def __init__(self, properties_parser):
        self.properties = properties_parser.load()
        self.repo = pygit2.init_repository(self.properties.root, False, initial_head='main')
        self.ignores = self.properties.ignores
        if not self.repo.references.get("refs/heads/main"):
            logging.debug(f"Initializing new git repository: [root={self.properties.root}]...")
            self.repo.index.write()
            tree = self.repo.index.write_tree()
            self.repo.create_commit("HEAD", self.me, self.me, "Baseline", tree, [])
            logging.info(f"Git repository initialized.")

    def inspect(self):
        logging.debug(f"Starting to inspect the repository changes...")
        current_statuses_dict = self.need_sync()
        if current_statuses_dict:
            message = create_commit_message(current_statuses_dict)
            (repo := self.repo).index.write()
            tree = repo.index.write_tree()
            parents = [repo.head.target]
            ref = repo.head.name
            repo.create_commit(ref, self.me, self.me, message, tree, parents)
        logging.info(f"Finish inspecting.")

    def log(self):
        print(f"\nHISTORY FOR {self.properties.root}:\n")
        last = self.repo[self.repo.head.target]
        for commit in self.repo.walk(last.id, pygit2.GIT_SORT_TIME):
            time = datetime.utcfromtimestamp(commit.commit_time).strftime('%Y-%m-%d %H:%M:%S')
            formatted_message = commit.message.replace("\n", "\n".ljust(len(time) + 3))
            print(f"{time}: {formatted_message}")

    def is_ignored_file(self, filepath):
        for pattern in self.ignores:
            if fnmatch.fnmatch(filepath, pattern):
                logging.debug(f"Changes is found but ignored: file={filepath}, pattern={pattern}.")
                return True
        return False

    def need_sync(self):
        status = self.repo.status()
        files_dict = {}

        for filepath, flags in status.items():
            if flags != GIT_STATUS_CURRENT and not self.is_ignored_file(filepath):
                if Path(self.properties.root, filepath).exists():
                    self.repo.index.add(filepath)
                logging.debug(f"Changes is found: file={filepath}.")
                files_dict[filepath] = map_status(flags)

        return files_dict
